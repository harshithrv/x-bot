# X - BOT
Create a post for X using google gemini and posts it to the X

# Prerequisites
1. Node.js v20
2. Twitter Developer Account
3. Google Gemini API Key

# Setting up the app locally

1. Clone the repository
2. cd to project
```bash
cd x-bot
```
3. Install the dependencies
```bash
npm install
```
4. Create a env folder and add the following
```env
mkdir env
```
5. Create a dev.env file inside the env folder and add the following
```env
API_KEY=YOUR_TWITTER_API_KEY
API_SECRET=YOUR_TWITTER_API_SECRET
BEARER_TOKEN=YOUR_TWITTER_BEARER_TOKEN
ACCESS_TOKEN=YOUR_TWITTER_ACCESS_TOKEN
ACCESS_TOKEN_SECRET=YOUR_TWITTER_ACCESS_TOKEN_SECRET
CLIENT_ID_OAUTH_TWO_POINT_O=YOUR_TWITTER_OAUTH2.O_CLIENT_ID
CLIENT_SECRET_OAUTH_TWO_POINT_O=YOUR_TWITTER_OAUTH2.O_CLIENT_SECRET
GEMINI_API_KEY=YOUR_GOOGLE_GEMINI_API_KEY
AUTH_SECRET=AUTH_SECRET_OF_YOUR_CHOICE
```
6. Run the app in development mode
```bash
npm run dev
```
### Other options to run the app once above setup steps are done
1. Running to test if posts are being created.
 - First make sure to build the app
    ```bash
    npm run build
    ```
    ```bash
    npm run cron
    ```
2. Running in prod mode (will not work unless environment variables are set)
```bash
npm start
```