import selectPostToPublish from "./selectPostToPublish";
import postToX from "./x/postToX";

async function main() {
	const post = await selectPostToPublish();
	if (post) {
		const response = await postToX({ text: post });
		if (response) console.log("Success");
		return;
	}

	console.log("Failed");
}

main();
