enum GeneratedBy {
    GEMINI = " ▶︎ Gemini",
    LLMA = " ▶︎ LLMA"
}

export default GeneratedBy;