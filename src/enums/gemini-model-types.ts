enum GeminiModel {
    GEMINI_1_0_PRO = "gemini-1.0-pro",
    GEMINI_1_5_PRO = "gemini-1.5-pro",
}

export default GeminiModel;