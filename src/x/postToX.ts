import moment from "moment";
import { TwitterApi } from "twitter-api-v2";

export interface PostData {
	text: string;
}

export interface CreatePostResponse {
	edit_history_tweet_ids?: Array<string>;
	id: string;
	text: string;
}

export default async function postToX(
	data: PostData
): Promise<CreatePostResponse | null> {
	try {
		const client = new TwitterApi({
			appKey: process.env.API_KEY as string,
			appSecret: process.env.API_SECRET as string,
			accessToken: process.env.ACCESS_TOKEN as string,
			accessSecret: process.env.ACCESS_TOKEN_SECRET as string,
		});

		const rwClient = client.readWrite;

		const postTweetResponse = await rwClient.v2.tweet(data.text);

		if (postTweetResponse)
			console.log("✅ Posted on X: ", data.text);

		return postTweetResponse.data;
	} catch (error) {
		console.log("❌ Failed posting on X");
		console.error("❌ Error postToX: ", error);
		return null;
	} finally {
		console.log("🥂 Finished at: ", moment().format("MMMM Do YYYY, h:mm:ss a"));
	}
}
