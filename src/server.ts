import express, { Express, Request, Response } from "express";
import selectPostToPublish from "./selectPostToPublish";
import postToX from "./x/postToX";

const app: Express = express();
const PORT = process.env.PORT || 8500;
const startTime = new Date();

app.route("/status").get((_req: Request, res: Response) => {
	const uptime = new Date().getTime() - startTime.getTime();
	return res.status(200).json({ message: "Server is up", uptime });
});

app.route("/post-to-x").post(async (req: Request, res: Response) => {
	const authHeader = req.headers.authorization;
	const token = authHeader?.split("Bearer ")[1];

	if (token === process.env.AUTH_SECRET) {
		const post = await selectPostToPublish();

		if (post) {
			const response = await postToX({ text: post });
			if (response) return res.status(200).json(response);
		}

		return res.status(403).json({ message: "❌ Failed to post" });
	}

	return res.status(401).json({ message: "❌ Unauthorized" });
});

app.listen(PORT, () => {
	console.log("🚀 Server is running on port:", PORT);
});
