import {
	GoogleGenerativeAI,
	HarmCategory,
	HarmBlockThreshold,
} from "@google/generative-ai";
import moment from "moment";
import GeminiModel from "../enums/gemini-model-types";
import MASTER_PROMPT from "../prompts/master";
import GeneratedBy from "../enums/generated-by-types";

const API_KEY = process.env.GEMINI_API_KEY as string;

export async function generatePostGemini(): Promise<string | null> {
	try {
		const genAI = new GoogleGenerativeAI(API_KEY);
		const model = genAI.getGenerativeModel({ model: GeminiModel.GEMINI_1_5_PRO });

		const generationConfig = {
			temperature: 0.9,
			topK: 1,
			topP: 1,
			maxOutputTokens: 300,
		};

		const safetySettings = [
			{
				category: HarmCategory.HARM_CATEGORY_HARASSMENT,
				threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
			},
			{
				category: HarmCategory.HARM_CATEGORY_HATE_SPEECH,
				threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
			},
			{
				category: HarmCategory.HARM_CATEGORY_SEXUALLY_EXPLICIT,
				threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
			},
			{
				category: HarmCategory.HARM_CATEGORY_DANGEROUS_CONTENT,
				threshold: HarmBlockThreshold.BLOCK_MEDIUM_AND_ABOVE,
			},
		];

		const parts = [
			{
				text: MASTER_PROMPT,
			},
		];

		console.log("🚀 Generating post via Gemini at: ", moment().format("MMMM Do YYYY, h:mm:ss a"));

		const result = await model.generateContent({
			contents: [{ role: "user", parts }],
			generationConfig,
			safetySettings,
		});

		const response = result.response;

		console.log("✅ Post created via Gemini: ", response.text());

		return response.text() + GeneratedBy.GEMINI;
	} catch (error) {
		console.log("❌ Failed creating post via Gemini");
		console.error("❌ Error generatePostGemini: ", error);
		return null;
	}
}
