import Groq from "groq-sdk";
import MASTER_PROMPT from "../prompts/master";
import LlamaModel from "../enums/llma-model-types";
import GeneratedBy from "../enums/generated-by-types";
import moment from "moment";

const getGroqChatCompletion = () => {
    const groq = new Groq({ apiKey: process.env.GROQ_API_KEY });

    return groq.chat.completions.create({
        messages: [
            {
                role: "user",
                content: MASTER_PROMPT,
            },
        ],
        model: LlamaModel.LLMA3_8B_8192,
    });

}
export default async function generatePostLlma(): Promise<string | null> {
    try {
        console.log("🚀 Generating post via Llma at: ", moment().format("MMMM Do YYYY, h:mm:ss a"));

        const chatCompletion = await getGroqChatCompletion();
        const response = chatCompletion.choices[0]?.message?.content + GeneratedBy.LLMA;

        if (response) {
            console.log("✅ Post created via Llma: ", response);
            return response;
        }

        throw new Error("❌ Failed to generate post via Llma");
    } catch (error) {
        console.error("❌ Error generatePostLlma: ", error);
        return null;
    }
}
