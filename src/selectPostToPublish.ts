import { generatePostGemini } from "./llm/gemini";
import generatePostLlma from "./llm/groq_llma";
import moment from "moment";

export default async function selectPostToPublish(): Promise<string | null> {
	try {
		console.log("🚀 Started generating post: ", moment().format("MMMM Do YYYY, h:mm:ss a"));

		const results = await Promise.allSettled([
			generatePostGemini(),
			generatePostLlma()
		]);

		const posts = results.map(result => {
			if (result.status === "fulfilled") {
				return result.value;
			}
			return null;
		})

		const [postGeneratedByGemini, postGeneratedByLlma] = posts;

		return selectPost(postGeneratedByGemini, postGeneratedByLlma);
	} catch (error) {
		console.log("❌ Failed to create post.");
		console.error("❌ Error selectPostToPublish: ", error);
		return null;
	}
}

const selectPost = (postA: string | null, postB: string | null): string | null => {
	if (postA && postB) {
		return Math.random() < 0.5 ? postA : postB;
	}
	return postA || postB || null;
}