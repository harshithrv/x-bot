const MASTER_PROMPT = `Create a concise, engaging, and informative tweet about a cutting-edge web development topic. Focus on recent trends, best practices, or innovative techniques in areas such as frontend frameworks (React, Vue, Angular), backend technologies (Node.js, Deno, Bun, Spring Boot), web performance optimization, progressive web apps, or emerging web standards.
	  
						Requirements:
						1. Maximum 280 characters
						2. Include at least two relevant hashtags (no time/date-related tags)
						3. Provide a specific, actionable insight or tip
						4. Mention a specific technology, library, or tool
						5. If applicable, include a brief code snippet or syntax example
						6. Aim to spark curiosity or encourage further exploration of the topic
						
						Example output:
						"🚀 Elevate your React game with the useCallback hook! Optimize performance by memoizing callback functions:

						const memoizedCallback = useCallback(
						() => doSomething(a, b),
						[a, b],
						);

						Prevents unnecessary re-renders in child components. #ReactJS #PerformanceTips"
						
						Now, generate a tweet following these guidelines, your response should only include the tweet:` as const;

export default MASTER_PROMPT;